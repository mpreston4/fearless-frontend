import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import NewConference from './new-conference';
import AttendConferenceForm from './attend-conference';
import PresentationForm from './PresentationForm';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from './MainPage';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path='/'>
            <Route index element={<MainPage />} />
          </Route>
          <Route path="/locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="/conferences">
            <Route path="new" element={<NewConference />} />
          </Route>
          <Route path="/attendees">
            <Route index element={<AttendeesList attendees={props.attendees} />} />
            <Route path="new" element={<AttendConferenceForm />} />
          </Route>
          <Route path="/presentations">
            <Route path="new" element={<PresentationForm />} />
          </Route>
        </Routes>

      </div>
    </BrowserRouter >
  );
}

export default App;
