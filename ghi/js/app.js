function createCard(name, description, pictureUrl, dateStartFinal, dateEndsFinal, location) {
    return `
        <div class="col">
            <div class="card" style="box-shadow: 10px 10px 5px; margin: 10px;">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <p class="card-subtitle mb-2 text-muted">${location}</p>
                <p class="card-text">${description}</p>
                <div class="card-footer">${dateStartFinal}-${dateEndsFinal}</div>
            </div>

        </div>
    `;
}




window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        // initial request to get some data. think of it like an agreement
        // think of it like an agreement between to parties to exchange data.
        // but the data isn't immediately returned
        const response = await fetch(url);

        if (!response.ok) {
            // Figure out what to do when the response is bad
            console.error("HTTP error " + response.status)
            showError("error fetching data: ")


        } else {
            // response is ok
            // we're awaiting the data to come back before doing something
            const data = await response.json();
            // const columns = document.querySelectorAll('.col')

            // cycle = 0;
            // we have the data we need, we need to sort through it
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const dateStartString1 = details.conference.starts
                    const dateStartString2 = new Date(dateStartString1)
                    const dateStartFinal = dateStartString2.toLocaleDateString();
                    const dateEnds1 = details.conference.ends
                    const dateEnds2 = new Date(dateEnds1)
                    const dateEndsFinal = dateEnds2.toLocaleDateString()
                    const location = details.conference.location.name
                    const html = createCard(title, description, pictureUrl, dateStartFinal, dateEndsFinal, location);
                    const column = document.querySelector('.row');
                    column.innerHTML += html;
                    // innerHTML = anything in between opening and closing tags

                }

            }

        }
    } catch (e) {
        // Figure out what to do if an error is raised
        console.error("An error occurred:", e);
        showError("error processing data")

    }

});

function showError(message) {
    const alert = `
    <div class ="alert alert-primary" role="alert">${message}<div>
    `
    document.body.innerHTML += alert
}
