console.log("This has asdfloaded")

window.addEventListener('DOMContentLoaded', async () => {

    let locationURL = "http://localhost:8000/api/locations/"
    let locationResponse = await fetch(locationURL)
    if (locationResponse.ok) {
        const data = await locationResponse.json();
        console.log(data)
        let locationSelect = document.getElementById("location")
        console.log(locationSelect
        )
        for (const location of data.locations) {
            const option = document.createElement('option'); // This is a browser function
            option.innerHTML = location.name;
            option.value = location.id;
            locationSelect.appendChild(option);
        }
    }
    const formTag = document.getElementById("create-conference-form");
    formTag.addEventListener("submit", async event => {

        event.preventDefault();

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conferenceURL = 'http://localhost:8000/api/conferences/'
        console.log(json)
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                "Content-Type": 'application/json'
            }
        };
        const newConferenceResponse = await fetch(conferenceURL, fetchConfig);

        if (newConferenceResponse.ok) {
            formTag.reset();
            const newConference = await newConferenceResponse.json();
            console.log(newConference);
        };

    });
});
