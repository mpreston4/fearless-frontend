console.log("This has loaded")

window.addEventListener('DOMContentLoaded', async () => {

    // add event listener to form
    let statesURL = "http://localhost:8000/api/states/"
    // Response from url after request is made
    let statesResponse = await fetch(statesURL)
    if (statesResponse.ok) {
        const data = await statesResponse.json();
        let stateSelect = document.getElementById("state")
        console.log(stateSelect)

        for (const state of data.states) {
            const option = document.createElement('option'); // This is a browser function
            // Set the '.innerHTML' property of the option element to
            option.innerHTML = state.name;
            // we have to set option value to state.abbreviation b/c it's a value object
            // the value object is identified by the abbreviation
            // This is the unique value for the option that's created, which eventually
            // gets returned to the api
            option.value = state.abbreviation;
            // Append the option element as a child of the select tag
            stateSelect.appendChild(option);
        }
    }
    const formTag = document.getElementById("create-location-form");
    // adding event listener to "submit" button
    formTag.addEventListener("submit", async event => {

        event.preventDefault();

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const locationURL = 'http://localhost:8000/api/locations/'
        console.log(json)
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                "Content-Type": 'application/json'
            }
        };
        // fetching data from locations url, so we can POST here
        const newLocationResponse = await fetch(locationURL, fetchConfig);

        if (newLocationResponse.ok) {
            // This resets the form to it's original state
            formTag.reset();
            const newLocation = await newLocationResponse.json();
            console.log(newLocation);
        };

    });
});









// prevent post
// We're handling the "POST" for the data with the restfulAPI's so the browser doesn't need to.
// "event.preventDefault()" will allow us to tell the browser not do handle the form when
// the user hits the submit button.


// get element ID
// we're saving the element id so we can tie it do the event listener.
// getElementById returns a reference to "create-location-form"
// We could then access its content using the textContent


// This allws us to compile a set of key/value pairs with the form data
// This method (new FormData(someForm) retrieves a FormData objet from an HTML form)
// FormData Object requires name attributes on the form inputs
// (ex: name="name", name="state", etc.) This allows it to
// create a "FormData" object (dictionary)